module gitlab.com/Gustibimo/shipper-cli-consignment

go 1.14

require (
	gitlab.com/Gustibimo/shipper-service-consignment v0.0.0-20200410065420-e805ee790430
	google.golang.org/grpc v1.28.1
)
